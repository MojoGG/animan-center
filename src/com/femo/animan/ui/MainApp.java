package com.femo.animan.ui;

import com.femo.animan.backbone.mangafox.MangaFoxManga;
import com.femo.animan.backbone.mangafox.MangaFoxProvider;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Pagination;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainApp extends Application {
	public static Stage primaryStage;
	public static BorderPane rootLayout;

	private Pagination pagination;

	public MangaFoxProvider mangafox = MangaFoxProvider.getProvider();

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Mangafox Manga Overview");

		initRootLayout();

		showMangaOverview();

		this.primaryStage.setMaximized(true);
	}

	public int getItemPerPages() {
		return itemPerPages;
	}

	public void setItemPerPages(int itemPerPages) {
		this.itemPerPages = itemPerPages;
	}

	int itemPerPages;

	/**
	 * Initializes the root layout.
	 */
	public void initRootLayout() {
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
			rootLayout = (BorderPane) loader.load();

			// Show the scene containing the root layout.
			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public FlowPane createPage(int pageIndex) {
		FlowPane box = new FlowPane();
		box.setPadding(new Insets(10, 10, 10, 10));
		box.setVgap(10);
		box.setHgap(10);
		ExecutorService service = Executors.newFixedThreadPool(6);
		int page = pageIndex * getItemPerPages();
        List<MangaFoxManga> mangas = mangafox.getMangaList();
		for (int i = page; i < page + getItemPerPages(); i++) {
			service.execute(new MangaDownload(mangas.get(i), box));
		}
		service.shutdown();
		return box;
	}

	/**
	 * Shows the person overview inside the root layout.
	 */
	public void showMangaOverview() {
		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		int width = gd.getDisplayMode().getWidth();
		int height = gd.getDisplayMode().getHeight();

		setItemPerPages((int) (width/200 * height/(320)));

		pagination = new Pagination(mangafox.getMangaList().size()/getItemPerPages());
		pagination.setPageFactory((Integer pageIndex) -> createPage(pageIndex));

		pagination.setCache(true);

		rootLayout.setCenter(pagination);
	}

	/**
	 * Returns the main stage.
	 * @return
	 */
	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public static void main(String[] args) {
		launch(args);
	}
}
