package com.femo.animan.ui;

import com.femo.animan.backbone.mangafox.MangaFoxManga;
import com.femo.animan.backbone.mangafox.MangaFoxProvider;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Pagination;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Boing on 14.04.2015.
 */
public class LoadingScreen extends Application {
    private Stage primaryStage;
    private BorderPane rootLayout;

    private Pagination pagination;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.initStyle(StageStyle.UNDECORATED);

        initLoadingScreen();
        Executors.newSingleThreadExecutor().submit(() -> {
            MangaFoxProvider.getProvider();
            Platform.runLater(primaryStage::close);
            Platform.runLater(LoadingScreen.this::openMainApp);
        });
    }

    /**
     * Initializes the loading screen.
     */
    private void initLoadingScreen() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/loadingscreen.fxml"));
            rootLayout = (BorderPane) loader.load();

            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void openMainApp() {
        Stage stage = new Stage();
        MainApp mainApp = new MainApp();
        mainApp.start(stage);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
