package com.femo.animan.ui;

import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

/**
 * Created by Moritz Egger on 21.04.2015.
 */
public class PageDownload implements Runnable {

	private String url;
	private ImageView iv;

	public PageDownload(String pageurl, ImageView imageView){
		url = pageurl;
		iv = imageView;
	}


	@Override
	public void run() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
					Image i = new Image(url);
					iv.setImage(i);
					if (i.getHeight() > 1000) {
						iv.setPreserveRatio(true);
						iv.setFitHeight(950);
					}

			}
		});
	}
}
