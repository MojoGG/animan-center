package com.femo.animan.ui.view;

import com.femo.animan.backbone.mangafox.MangaFoxChapter;
import com.femo.animan.ui.PageDownload;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Pagination;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Moritz Egger on 14.04.2015.
 */
public class ChapterViewController {

	public static boolean ReadPages = true;

	@FXML
	private AnchorPane anchorPane;

	@FXML
	private ScrollPane scrollPane;

	private ArrayList<String> chapters;

	@FXML
	private void initialize() {

	}

	public void viewChapter(MangaFoxChapter chapter){
		chapter.loadPageCount();
		if(ChapterViewController.ReadPages) {
			Pagination pagination = new Pagination(chapter.getPagescount(), 0);
			pagination.setPageFactory((Integer pageIndex) -> createPage(pageIndex, chapter));
			pagination.autosize();
			pagination.setCache(true);

			scrollPane.setContent(pagination);
		}else if(!ChapterViewController.ReadPages){
			final FlowPane group = new FlowPane();
			scrollPane.setContent(group);
			VBox.setVgrow(scrollPane, Priority.ALWAYS);
			Task<Void> task = new Task<Void>() {
				@Override protected Void call() throws Exception {
					System.out.println("Call");
					for (int i=0; i<chapter.getPagescount(); i++) {
						if (isCancelled()){ System.out.println("break"); break;}
						Image im = new Image(chapter.loadSingleImage(i+1));
						ImageView iv = new ImageView(im);
						if (im.getHeight() > 1000) {
							iv.setPreserveRatio(true);
							iv.setFitHeight(950);
						}
						System.out.println("Loaded Image");
						Platform.runLater(new Runnable() {
							@Override
							public void run() {
								group.getChildren().add(iv);
								System.out.println(group.getChildren().size());
							}
						});
					}
					return null;
				}
			};
			Thread thread = new Thread(task);
			thread.setDaemon(true);
			thread.start();
			scrollPane.setContent(group);
		}
	}

	private Node createPage(Integer pageIndex, MangaFoxChapter chapter) {
		//System.out.println(chapters.get(pageIndex));
		ExecutorService service = Executors.newSingleThreadExecutor();
		BorderPane borderPane = new BorderPane();
		ImageView iv = new ImageView();
		service.execute(new PageDownload(chapter.loadSingleImage(pageIndex+1), iv));
		service.shutdown();
		borderPane.setCenter(iv);
		return borderPane;
	}

}
