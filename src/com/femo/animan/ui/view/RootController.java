package com.femo.animan.ui.view;

import com.femo.animan.ui.MainApp;
import javafx.fxml.FXML;

/**
 * Created by Moritz Egger on 15.04.2015.
 */
public class RootController {
	@FXML
	public void openMainApp(){
		MainApp main = new MainApp();
		main.showMangaOverview();
	}

	@FXML
	public void searchKey(){

	}
}
