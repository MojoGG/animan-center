package com.femo.animan.ui.view;

import com.femo.animan.backbone.mangafox.MangaFoxChapter;
import com.femo.animan.backbone.mangafox.MangaFoxManga;
import com.femo.animan.ui.MainApp;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

/**
 * Created by Moritz Egger on 13.04.2015.
 */
public class MangaViewController {
	@FXML
	private Label mangaName;
	@FXML
	private Label mangaDesc;
	@FXML
	private ImageView mangaCover;

	@FXML
	private ScrollPane scroll;

	@FXML
	private void initialize() {

	}

	public void showMangaDetails(MangaFoxManga manga){
		mangaName.setText(manga.getName());
		Image i = new Image(manga.getCover());
		mangaCover.setImage(i);

		manga.loadDesc();
		mangaDesc.setText(manga.getDesc());

		//Load Chapters
		manga.loadChapters();
		ArrayList<MangaFoxChapter> chapters = manga.getChapters();

		FlowPane flow = new FlowPane();

		int t = 0;

		for(MangaFoxChapter c : chapters){
			HBox hbox = new HBox();
			HBox hboxleft = new HBox();
			HBox hboxright = new HBox();
			Label name = new Label(c.getName());
			hboxleft.getChildren().add(name);

			hbox.setPrefWidth(1000);
			hbox.setPrefHeight(50);

			if(t%2==0)
				hbox.setStyle("-fx-background-color: #3B3B3B;");
			else
				hbox.setStyle("-fx-background-color: #575757");

			Button openInBrowser = new Button("Open in Browser");
			Button openInReader = new Button("Open in Reader");

			openInReader.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					FXMLLoader loader = new FXMLLoader();
					loader.setLocation(MainApp.class.getResource("view/chapterview.fxml"));
					AnchorPane anchorPane = null;
					try {
						anchorPane = (AnchorPane) loader.load();
					} catch (IOException e) {
						e.printStackTrace();
					}
					ChapterViewController controller = loader.getController();
					controller.viewChapter(c);
					Scene scene = new Scene(anchorPane);
					MainApp.rootLayout.setCenter(anchorPane);

					Stage primaryStage = MainApp.primaryStage;
					primaryStage.setTitle("Manga - " + manga.getName() + "- Chapter - " + c.getName());
					primaryStage.show();
				}
			});

			openInBrowser.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					if(Desktop.isDesktopSupported())
					{
						try {
							Desktop.getDesktop().browse(new URI(c.getUrl()+"1.html"));
						} catch (IOException e) {
							e.printStackTrace();
						} catch (URISyntaxException e) {
							e.printStackTrace();
						}
					}
				}
			});

			hboxright.getChildren().add(openInBrowser);
			hboxright.getChildren().add(openInReader);

			hboxleft.setAlignment(Pos.CENTER_LEFT);
			hboxright.setAlignment(Pos.CENTER_RIGHT);
			hboxleft.setMargin(hboxleft,new Insets(0, 0, 0, 25));
			hboxright.setMargin(hboxright,new Insets(0, 25, 0, 0));
			hboxleft.setHgrow(hboxleft, Priority.ALWAYS);
			hboxright.setHgrow(hboxright, Priority.ALWAYS);

			hbox.getChildren().add(hboxleft);
			hbox.getChildren().add(hboxright);
			flow.getChildren().add(hbox);
			t++;
		}
		scroll.setContent(flow);
	}
}
