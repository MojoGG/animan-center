package com.femo.animan.ui;

import com.femo.animan.backbone.mangafox.MangaFoxChapter;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;


/**
 * Created by Moritz Egger on 28.04.2015.
 */
public class ChapterDownload implements Runnable{

	MangaFoxChapter chapter;
	VBox vBox;

	public ChapterDownload(MangaFoxChapter c,VBox v){
		chapter = c;
		vBox = v;
	}

	@Override
	public void run() {
		for (int i = 0; i < chapter.getPagescount(); i++) {
			final int temp = i+1;
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					chapter.loadPageCount();
						System.out.println(temp);
						Image im = new Image(chapter.loadSingleImage(temp));
						final ImageView iv = new ImageView(im);
						vBox.getChildren().add(iv);
				}
			});
		}
	}
}
