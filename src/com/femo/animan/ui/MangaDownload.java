package com.femo.animan.ui;

import com.femo.animan.backbone.mangafox.MangaFoxManga;
import com.femo.animan.backbone.mangafox.MangaFoxProvider;
import com.femo.animan.ui.view.MangaViewController;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by Boing on 13.04.2015.
 */
public class MangaDownload implements Runnable {

    private MangaFoxManga manga;
    private FlowPane panel;

    public MangaDownload(MangaFoxManga manga, FlowPane panel) {
        this.manga = manga;
        this.panel = panel;
    }

    @Override
    public void run() {
        VBox vox = new VBox();
        manga.loadCover();
        Image im = new Image(manga.getCover());
        ImageView iv = new ImageView(im);
        iv.setFitWidth(100);
        iv.setPreserveRatio(true);
        Button b = new Button("",iv);
        b.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(MainApp.class.getResource("view/mangaview.fxml"));
                AnchorPane anchorPane = null;
                try {
                    anchorPane = (AnchorPane)loader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                MangaViewController controller = loader.getController();
                controller.showMangaDetails(manga);
                Scene scene = new Scene(anchorPane);

                MainApp.rootLayout.setCenter(anchorPane);

                Stage primaryStage = MainApp.primaryStage;
                primaryStage.setTitle("Manga Site - " + manga.getName());
                primaryStage.show();
            }
        });
        vox.getChildren().add(b);

        Label l = new Label();
        l.setText(manga.getName());
        vox.getChildren().add(l);

        vox.setAlignment(Pos.CENTER);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                panel.getChildren().add(vox);
            }
        });
    }
}
