package com.femo.animan.backbone.mother;

/**
 * Created by Moritz Egger on 14.04.2015.
 */
public class Genre {
	String genreName;

	public String getDirectoryUrl() {
		return directoryUrl;
	}

	public void setDirectoryUrl(String directoryUrl) {
		this.directoryUrl = directoryUrl;
	}

	public String getGenreName() {
		return genreName;
	}

	public void setGenreName(String genreName) {
		this.genreName = genreName;
	}

	String directoryUrl;


	public Genre(String name, String url){
		setGenreName(name);
		setDirectoryUrl(url);
	}

}
