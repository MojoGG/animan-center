package com.femo.animan.backbone.mangafox;

import com.femo.animan.backbone.html.HTMLHandler;
import com.femo.animan.backbone.mother.Provider;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

/**
 * Created by Moritz Egger on 12.04.2015.
 */
public class MangaFoxProvider{

	private static MangaFoxProvider provider;

	public final String Name = "mangafox.me";

	public ArrayList<MangaFoxManga> getMangaList() {
		return mangaList;
	}

	public void setMangaList(ArrayList<MangaFoxManga> mangaList) {
		this.mangaList = mangaList;
	}

	ArrayList<MangaFoxManga> mangaList;

	private MangaFoxProvider(){
		setMangaList(new ArrayList<MangaFoxManga>());
		loadMangaList();
	}

	public void loadCover(){
		for(MangaFoxManga ma : getMangaList()){
			ma.loadCover();
		}
	}

	public void loadMangaList(){
		mangaList.clear();
		Document doc = Jsoup.parse(new HTMLHandler().getHTMLFromURL("http://www.mangafox.me/manga/"));
		Element list = doc.getElementsByClass("left").get(4);
		Elements allManga = list.getElementsByClass("manga_list");
		for(Element e : allManga){
			Elements mangas = e.getElementsByAttribute("rel");
			for(Element t : mangas){
				String manganame = t.ownText();
				String[] s = t.attr("href").split("/");
				String id = s[s.length-1];
				MangaFoxManga manga = new MangaFoxManga(id,manganame);
				getMangaList().add(manga);
			}
		}
	}

	public static MangaFoxProvider getProvider() {
		if (provider == null) {
			provider = new MangaFoxProvider();
		}
		return provider;
	}
}
