package com.femo.animan.backbone.mangafox;

import com.femo.animan.backbone.html.HTMLHandler;
import com.femo.animan.backbone.mother.Genre;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

/**
 * Created by Moritz Egger on 12.04.2015.
 */
public class MangaFoxManga {
	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	String desc;

	public String getCover() {
		return cover;
	}

	public void setCover(String cover) {
		this.cover = cover;
	}

	String cover;
	String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<MangaFoxChapter> getChapters() {
		return chapters;
	}

	public void setChapters(ArrayList<MangaFoxChapter> chapters) {
		this.chapters = chapters;
	}

	ArrayList<MangaFoxChapter> chapters;
	String id;

	public Document getMangaDoc() {
		return MangaDoc;
	}

	public void setMangaDoc(Document mangaDoc) {
		MangaDoc = mangaDoc;
	}

	Document MangaDoc;

	public ArrayList<Genre> getGenreList() {
		return genreList;
	}

	public void setGenreList(ArrayList<Genre> genreList) {
		this.genreList = genreList;
	}

	ArrayList<Genre> genreList = new ArrayList<Genre>();

	public MangaFoxManga(String mangaId){
		setId(mangaId);
		setMangaDoc(Jsoup.parse(new HTMLHandler().getHTMLFromURL("http://mangafox.me/manga/" + getId())));

		Elements mangatitle = MangaDoc.select("meta[property=og:title]");
		String content = mangatitle.attr("content");

		setName(content);
		setChapters(new ArrayList<MangaFoxChapter>());
	}

	public MangaFoxManga(String mangaId, String mangaName){
		setId(mangaId);
		setName(mangaName);
		setChapters(new ArrayList<MangaFoxChapter>());
	}

	public void loadGenre(){
		if(getMangaDoc()==null)
			setMangaDoc(Jsoup.parse(new HTMLHandler().getHTMLFromURL("http://mangafox.me/manga/" + getId())));
		Element titleElement = getMangaDoc().getElementById("title");
		Element table = titleElement.getElementsByTag("table").first();
		Element tr = table.getElementsByTag("tr").get(1);
		Element td = tr.getElementsByTag("td").get(3);
		Elements href = td.getElementsByAttribute("href");
		for(Element e : href){
			Genre g = new Genre(e.ownText(),e.attr("href"));
			getGenreList().add(g);
		}
	}

	public void loadDesc(){
		if(getMangaDoc()==null)
			setMangaDoc(Jsoup.parse(new HTMLHandler().getHTMLFromURL("http://mangafox.me/manga/" + getId())));
		Element mangadesc = getMangaDoc().getElementsByClass("summary").first();
		if(mangadesc == null)
			setDesc("No Description available");
		else
			setDesc(mangadesc.ownText());
	}

	public void loadChapters(){
		if(getMangaDoc()==null)
			setMangaDoc(Jsoup.parse(new HTMLHandler().getHTMLFromURL("http://mangafox.me/manga/" + getId())));
		Element chap = MangaDoc.getElementById("chapters");
		Elements allchapt = chap.getElementsByClass("chlist");
		for(Element e : allchapt){
			Elements tips = e.getElementsByClass("tips");
			for(Element t : tips){
				MangaFoxChapter cha = new MangaFoxChapter(t.attr("href"),t.ownText());
				getChapters().add(cha);
			}
		}
	}

	public void loadCover(){
		if(getMangaDoc()==null)
			setMangaDoc(Jsoup.parse(new HTMLHandler().getHTMLFromURL("http://mangafox.me/manga/" + getId())));
		Element mangacover = MangaDoc.getElementsByClass("Cover").first();
		setCover(mangacover.getElementsByAttribute("src").first().attr("src"));
	}
}
