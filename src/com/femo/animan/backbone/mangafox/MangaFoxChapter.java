package com.femo.animan.backbone.mangafox;

import com.femo.animan.backbone.html.HTMLHandler;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

/**
 * Created by Moritz Egger on 12.04.2015.
 */
public class MangaFoxChapter {
	String name;
	String url;

	public ArrayList<String> getImageUrls() {
		return imageUrls;
	}

	public void setImageUrls(ArrayList<String> imageUrls) {
		this.imageUrls = imageUrls;
	}

	ArrayList<String> imageUrls;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MangaFoxChapter(String chapturl){
		splitUrl(chapturl);
	}

	public int getPagescount() {
		return pagescount;
	}

	public void setPagescount(int pagescount) {
		this.pagescount = pagescount;
	}

	private int pagescount;

	private void splitUrl(String chapturl) {
		String[] s = chapturl.split("/");
		String a = "";
		for(int i = 0; i < s.length - 1;i++){
			a += s[i] + "/";
		}
		setUrl(a);
	}

	public MangaFoxChapter(String chapturl, String name){
		splitUrl(chapturl);
		setName(name);
	}

	public void loadPageCount(){
		Document doc = Jsoup.parse(new HTMLHandler().getHTMLFromURL(url));

		Element select = doc.getElementsByClass("m").first();
		Elements option = select.select("select > option");
		setPagescount(Integer.parseInt(option.get(option.size() - 2).ownText()));
	}

	public String loadSingleImage(int page){
		String pageurl = url + page + ".html";
		Document imagedox = Jsoup.parse(new HTMLHandler().getHTMLFromURL(pageurl));
		Element image = imagedox.getElementById("image");
		return image.attr("src");
	}

	public void loadImages(){
		setImageUrls(new ArrayList<String>());
		for(int i = 1; i <= pagescount; i++){
			String pageurl = url + i + ".html";
			Document imagedox = Jsoup.parse(new HTMLHandler().getHTMLFromURL(pageurl));
			Element image = imagedox.getElementById("image");
			getImageUrls().add(image.attr("src"));
		}
	}
}
