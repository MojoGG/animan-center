package com.femo.animan.backbone.html;


import com.mashape.unirest.http.Unirest;

/**
 * Created by Moritz Egger on 12.04.2015.
 */
public class HTMLHandler {

	public String getHTMLFromURL(String url){
		String content = null;
		try {
			content = Unirest.get(url).asString().getBody();
		}catch ( Exception ex ) {
			ex.printStackTrace();
		}
		return content;
	}
}
