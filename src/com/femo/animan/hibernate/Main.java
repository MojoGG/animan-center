package com.femo.animan.hibernate;

import com.femo.animan.hibernate.controller.MangaController;
import com.femo.animan.hibernate.controller.ProviderController;
import com.femo.animan.hibernate.controller.UserController;
import com.femo.animan.hibernate.model.*;

import java.util.List;
import java.util.Set;

/**
 * Created by Momo on 05.05.2015.
 */
public class Main {
    public static void main(final String[] args) throws Exception {
        UserController uc = new UserController();
        System.out.println("|||||CREATE User||||||");
        uc.addUser("TestUser1", "TestPw1");
        System.out.println("|||||ADD User||||||");
        List l = uc.getAllUser();
        for(Object li : l){
            System.out.println(((User)li).getUsername());
        }
        System.out.println("|||||GET SINGLE User||||||");
        System.out.println(uc.getSingleUser("TestUser1").getUsername());
        System.out.println("|||||DELETE User||||||");
        uc.deleteUser("TestUser1");
        System.out.println("|||||Get All User||||||");
        l = uc.getAllUser();
        for(Object li : l){
            System.out.println(((User)li).getUsername());
        }
        MangaController mc = new MangaController();
        ProviderController pc = new ProviderController();

        List ml = mc.getAllMangas();
        for(Object o : ml){
            Manga m = (Manga) o;
            Set<User> fav = m.getFavs();
            for(User u : fav){
                System.out.println(m.getName() + " " + u.getUsername());
            }
        }

        System.out.println(mc.getSingleManga("Test2").getName());
    }
}
