package com.femo.animan.hibernate.model;

/**
 * Created by Momo on 05.05.2015.
 */
public class Provider {
    private int id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private String name;

    public Provider(){}
    public Provider(String n){
        name = n;
    }
}
