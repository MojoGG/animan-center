package com.femo.animan.hibernate.model;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Momo on 05.05.2015.
 */
@XmlRootElement
public class Manga {
    private int id;
    private String name;

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Provider getProvider() {
        return provider;
    }

    private Provider provider;

    public Set<User> getFavs() {
        return favs;
    }

    public void setFavs(Set<User> favs) {
        this.favs = favs;
    }

    private Set<User> favs = new HashSet<User>();

    public Manga(){}
    public Manga(String name, Provider provider){
        this.name = name;
        this.provider = provider;
    }
}
