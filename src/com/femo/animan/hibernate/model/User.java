package com.femo.animan.hibernate.model;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Momo on 05.05.2015.
 */
public class User {
    private String username;
    private String password;

    public Set<Manga> getFavs() {
        return favs;
    }

    public void setFavs(Set<Manga> favs) {
        this.favs = favs;
    }

    private Set<Manga> favs = new HashSet<Manga>();

    public User(){}

    public User(String uname, String pword){
        username = uname;
        password = pword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
