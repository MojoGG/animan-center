package com.femo.animan.hibernate.controller;

import com.femo.animan.hibernate.model.Provider;
import com.femo.animan.hibernate.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import java.util.List;

/**
 * Created by Momo on 05.05.2015.
 */
public class ProviderController {
    private static final SessionFactory sessionFactory;
    private static final ServiceRegistry serviceRegistry;

    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();

            serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public void addProvider(String name){
        Session s = sessionFactory.openSession();
        Transaction tx = s.beginTransaction();
        Provider p = new Provider(name);
        s.save(p);
        tx.commit();
    }

    public int getIdWithName(String name){
        Session s = sessionFactory.openSession();
        Transaction tx = s.beginTransaction();

        Provider p = (Provider) s.createQuery("From Provider where name = " + name).list().get(0);

        return p.getId();
    }

    public Provider getSingleProvider(String uname){
        int i = getIdWithName(uname);
        Session s = sessionFactory.openSession();
        Transaction tx = s.beginTransaction();

        Provider p = (Provider) s.get(Provider.class,i);

        tx.commit();

        return p;
    }

    public Provider getSingleProvider(int id){
        Session s = sessionFactory.openSession();
        Transaction tx = s.beginTransaction();

        Provider p = (Provider) s.get(Provider.class,id);

        tx.commit();

        return p;
    }

    public List getAllProvider(){
        Session s = sessionFactory.openSession();
        Transaction tx = s.beginTransaction();

        List providers = s.createQuery("From Provider ").list();

        tx.commit();

        return providers;
    }

    public void deleteProvider(String uname){
        int id = getIdWithName(uname);
        Session s = sessionFactory.openSession();
        Transaction tx = s.beginTransaction();

        Provider p = (Provider) s.get(Provider.class,id);
        s.delete(p);
        tx.commit();
    }
}
