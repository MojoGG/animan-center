package com.femo.animan.hibernate.controller;

import com.femo.animan.hibernate.model.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.hibernate.Query;

import java.util.List;

/**
 * Created by Momo on 05.05.2015.
 */
public class MangaController {
    private static final SessionFactory sessionFactory;
    private static final ServiceRegistry serviceRegistry;

    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();

            serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public void addManga(String name, Provider provider){
        Session s = sessionFactory.openSession();
        Transaction tx = s.beginTransaction();
        Manga m = new Manga(name,provider);
        s.save(m);
        tx.commit();
    }

    public int getIdWithName(String name){
        Session s = sessionFactory.openSession();
        Transaction tx = s.beginTransaction();


        List<Manga> ml = s.createQuery("From Manga m where m.name like '" + name + "'").list();
        if(ml.size() == 0)
            return -1;

        Manga m = ml.get(0);
        return m.getId();
    }

    public Manga getSingleManga(String uname){
        int id = getIdWithName(uname);
        Session s = sessionFactory.openSession();

        Query q = s.createQuery("FROM Manga where id = :id");
        q.setParameter("id", id);
        Manga m = (Manga)q.list().get(0);

        return m;
    }

    public Manga getSingleManga(int id){
        Session s = sessionFactory.openSession();

        Query q = s.createQuery("FROM Manga where id = :id");
        q.setParameter("id",id);
        Manga m = (Manga)q.list().get(0);

        return m;
    }

    public List<Manga> getAllMangas(){
        Session s = sessionFactory.openSession();
        Transaction tx = s.beginTransaction();

        List mangas = s.createQuery("From Manga").list();
        tx.commit();

        return mangas;
    }

    public void deleteManga(String uname){
        int id = getIdWithName(uname);
        Session s = sessionFactory.openSession();
        
        Query q = s.createQuery("DELETE FROM Manga where id = :id");
        q.setParameter("id", id);
        q.executeUpdate();
    }

    public void deleteManga(int id){
        Session s = sessionFactory.openSession();

        Query q = s.createQuery("DELETE FROM Manga where id = :id");
        q.setParameter("id",id);
        q.executeUpdate();
    }

    public void updateManga(Manga m){
        Session s = sessionFactory.openSession();

        Query query = s.createQuery("update Manga set name = :name, provider = :provider where id = :id");
        query.setParameter("name", m.getName());
        query.setParameter("provider", m.getProvider());
        query.setParameter("id", m.getId());
        query.executeUpdate();
    }
}
