package com.femo.animan.hibernate.controller;

import com.femo.animan.hibernate.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import java.util.List;

/**
 * Created by Momo on 05.05.2015.
 */
public class UserController {
    private static final SessionFactory sessionFactory;
    private static final ServiceRegistry serviceRegistry;

    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();

            serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public void addUser(String username, String password){
        Session s = sessionFactory.openSession();
        Transaction tx = s.beginTransaction();
        User u = new User(username,password);
        s.save(u);
        tx.commit();
    }

    public User getSingleUser(String uname){
        Session s = sessionFactory.openSession();
        Transaction tx = s.beginTransaction();

        User u = (User) s.get(User.class,uname);

        tx.commit();

        return u;
    }

    public List getAllUser(){
        Session s = sessionFactory.openSession();
        Transaction tx = s.beginTransaction();

        List users = s.createQuery("From User").list();

        tx.commit();

        return users;
    }

    public void deleteUser(String uname){
        Session s = sessionFactory.openSession();
        Transaction tx = s.beginTransaction();

        User u = (User) s.get(User.class,uname);
        s.delete(u);
        tx.commit();
    }
}
