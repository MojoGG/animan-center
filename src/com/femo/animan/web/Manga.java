package com.femo.animan.web;

import com.femo.animan.hibernate.controller.MangaController;
import com.femo.animan.hibernate.controller.ProviderController;
import com.femo.animan.hibernate.model.Provider;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/manga")
/**
 * Created by MojoGG on 6/15/15.
 */
public class Manga {
    @GET
    @Path("all")
    @Produces("application/json")
    public String retunAllManga(){
        JSONArray json = new JSONArray();
        MangaController mc = new MangaController();
        List ml = mc.getAllMangas();
        String s = "";
        for(Object o : ml){
            com.femo.animan.hibernate.model.Manga m = (com.femo.animan.hibernate.model.Manga) o;
            JSONObject j = new JSONObject();
            j.put("name",m.getName());
            j.put("id",m.getId());
            j.put("provider",m.getProvider().getName());
            json.put(j);
        }
        return json.toString();
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String getCount() {
        MangaController mc = new MangaController();
        int count = mc.getAllMangas().size();
        return String.valueOf(count);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.TEXT_PLAIN)
    public String deleteManga(@PathParam("id") int id){
        MangaController mc = new MangaController();
        mc.deleteManga(id);
        return "deleted";
    }

    @GET
    @Path("{manganame}")
    @Produces("application/json")
    public String getManga(@PathParam("manganame") String id) {
        System.out.println(id);
        MangaController mc = new MangaController();
        com.femo.animan.hibernate.model.Manga m = mc.getSingleManga(id);
        System.out.println(m.getName());
        JSONObject json = new JSONObject();
        json.put("name",m.getName());
        json.put("id",m.getId());
        json.put("provider",m.getProvider().getName());
        return json.toString();
    }

    @POST
    @Path("post")
    public String postManga(@QueryParam("name") String name,@QueryParam("provid") int provid){
        MangaController mc = new MangaController();
        ProviderController pc = new ProviderController();
        if(mc.getSingleManga(name) != null){
            com.femo.animan.hibernate.model.Manga m = mc.getSingleManga(name);
            m.setProvider(pc.getSingleProvider(provid));
            mc.updateManga(m);
            return "changed Provider from " + m.getName() + " to " + m.getProvider().getName();
        }
        Provider p = pc.getSingleProvider(provid);
        mc.addManga(name,p);
        com.femo.animan.hibernate.model.Manga m = mc.getSingleManga(name);
        return "added " + m.getName();
    }

    @PUT
    @Path("update")
    public String updateManga(@QueryParam("id") int id, @QueryParam("name") String name, @DefaultValue("-1")@QueryParam("provid") int provid){
        MangaController mc = new MangaController();
        ProviderController pc = new ProviderController();
        com.femo.animan.hibernate.model.Manga m = mc.getSingleManga(id);
        if(m == null)
            return "no manga found with id " + id;
        if(name != "" && m.getName() != name)
            m.setName(name);
        if(provid != -1  && m.getProvider().getId() != provid) {
            m.setProvider(pc.getSingleProvider(provid));
        }

        mc.updateManga(m);
        return "updated " + m.getId() + " to " + m.getName() + " " + m.getProvider().getName();
    }
}
