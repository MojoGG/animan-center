# README #

This README would normally document whatever steps are necessary to get your application up and running.
(--> Currently none, as the program isn't running right now ;D)
    --> Program is working now!

### What is AniMan Center? ###

Current Features:
*Pulling Manga List from Mangafox
*Reading single Manga Chapter in different reading modes
*Get Manga Information

AniMan Center is a central application for Anime & Manga. You can view your favourite Mangas, watch the newest Anime episodes and much more. Features will include:
* Marking a Manga as favourite (and get updates when a new Chapter is released)
* Remember the latest chapters you read
* Sync between your devices (via account or file)
* Read online on your android tablet or phone